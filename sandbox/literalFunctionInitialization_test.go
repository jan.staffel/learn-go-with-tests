package literalFunctionInitialization

import (
	"testing"
)

func TestInit(t *testing.T) {

	for i := 0; i < 10; i++ {
		t.Run("pre-declared function literal, fed with logic from the declaring function", func(t *testing.T) {
			expected := "ab"
			//return keyword seems to be protected, so i use testReturn. I also saw things like "aReturn" or "aMap", but i deeeeeeeeply despise that nomenclature
			testReturn := init1(i, expected)
			if testReturn != expected {
				t.Errorf("received %v return", testReturn)
			}
		})
	}

/*		t.Run("pre-declared function literal, fed by reflection", func(t *testing.T) {
//			t.Errorf("test")
			expected := "abcd"
//			t.Errorf(expected)
			testReturn, _ := init2(2, expected)
			t.Errorf("%+v", testReturn)
//			t.Errorf("%+v", testReturn)
//			if testReturn != expected {
//				t.Errorf("received %v, expected %v", testReturn, expected)
//			}
		})
*/
}
