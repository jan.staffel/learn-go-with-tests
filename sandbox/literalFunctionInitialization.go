package literalFunctionInitialization

import (
	"reflect"
//	"fmt"
)

//I just learned through trial and error that "func init()" is a protected function without parameters and return values that is executed when the package is either imported or before the main(), depending on context. So this is called init1().
//This functions purpose is to visualize how a function signature can be declared and then fulfilled by a function literal afterwards
func init1(iterator int, expected string) string {
	//declare the testInit variable with a function signature
	var testInit func(int) string
	//put some logic into the declared function
	testInit = func(a int) string {
		return expected
	}

	//see if it is correctly set. This seems like an easy and basic functionality, but might be a mighty functionality for run-time (read: dynamic) evaluation of constants or reflections.
	return (testInit(iterator))
}


//trying out the syntax from https://quii.gitbook.io/learn-go-with-tests/go-fundamentals/reflection#write-enough-code-to-make-it-pass-7 to figure out how this works. To no avail yet.
func init2(iterator int, expected string) (interface{}, interface{}) {
	var testInit func(int) reflect.Value
	value := reflect.ValueOf(iterator)
//	value2 := value.Kind()
//	testInit(1)
	testInit = value.Field
	testInitType := testInit(1)
	return testInit, testInitType
}
