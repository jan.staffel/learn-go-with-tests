#!/bin/bash
echo "start watching"
while WATCHFILE="$(inotifywait -r -e close_write --excludei "(.go\..*)|([0-9]+$)|(^\..*)\gm" -q . | head -n1 | awk '{print $1;}')" 
do 
echo ""
echo "-------------------------------------------------------"
echo "start vetting at $(date)"
go vet $WATCHFILE
echo "top vetting at $(date)"
echo "-------------------------------------------------------"
echo ""
echo "-------------------------------------------------------"
echo "start testing at $(date)"
go test -cover -bench=. $WATCHFILE -race
echo "stop testing at $(date)"
echo "-------------------------------------------------------"
done

