package maps

import "errors"

type Dictionary map[string]string

var ErrNotFoundVar = errors.New("could not find word")
var ErrWordExistsVar = errors.New("word already exists")

const (
	ErrNotFound = DictionaryErr("could not find word")
	ErrWordExists = DictionaryErr("word already exists")
	ErrWordDoesNotExist = DictionaryErr("word does not exist")
)

type DictionaryErr string

func (e DictionaryErr) Error() string {
	return string(e)
}

func (d Dictionary) Search(word string) (string, error) {
	definition, ok := d[word]

	if !ok {
		return "", ErrNotFound
	}

	return definition, nil
}

func (d Dictionary) Add(word string, definition string) error {

	_, err := d.Search(word)
/* Also works, but doesn't check for generic errors. Also doesn't take changes in the search method into account
	_, exists := d[word]
	if exists {
		return ErrWordExists
	}
*/
	switch err {
		case ErrNotFound:
			d[word] = definition
		case nil:
			return ErrWordExists
		default:
			return err
	}

	return nil
}

func (d Dictionary) Update(word, definition string) error {
	_, err := d.Search(word)

	switch err {
		case nil:
			d[word] = definition
		case ErrNotFound:
			return ErrWordDoesNotExist
		default:
			return err
	}

	return nil
}

func (d Dictionary) Delete(word string) {
	delete(d, word)
}
