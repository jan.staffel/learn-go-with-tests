package reflection

import (
	"testing"
	"reflect"
)

type testCase struct{
	Name string
	Input interface{}
	ExpectedCalls []string
}

type Person struct {
	Name string
	Profile Profile
}

type Profile struct {
	Age int
	City string
}

func assertContains(t testing.TB, haystack []string, needle string) {
	t.Helper()
	contains := false
	for _, x := range haystack {
		if x == needle {
			contains = true
			break
		}
	}

	if !contains {
		t.Errorf("expected %+v to contain %q but it didn't", haystack, needle)
	}
}

func TestWalk(t *testing.T) {
	var cases []testCase

	case1 := testCase{
		"Struct with one string field",
		struct {
			Name string
		} {"Jan"},
		[]string{"Jan"},
	}
	cases = append(cases, case1)

	case2 := testCase{
		"Struct with two string fields",
		struct {
			Name string
			City string
		}{"Jan", "Juelich"},
		[]string{"Jan", "Juelich"},
	}
	cases = append(cases, case2)

	case3 := testCase{
		"Struct with none string field",
		struct {
			Name string
			Age int
		}{"Jan", 34},
		[]string{"Jan"},
	}
	cases = append(cases, case3)

	case4 := testCase{
		"Nested fields",
		struct {
			Name string
			Profile struct {
				Age int
				City string
			}
		}{"Jan", struct {
			Age int
			City string
		}{34, "Juelich"}},
		[]string{"Jan", "Juelich"},
	}
	cases = append(cases, case4)

	case5 := testCase{
		"Nested fields",
		Person{
			"Jan",
			Profile{34, "Juelich"},
		},
		[]string{"Jan", "Juelich"},
	}
	cases = append(cases, case5)

	case6 := testCase{
		"Pointers to things",
		&Person{
			"Jan",
			Profile{34, "Juelich"},
		},
		[]string{"Jan", "Juelich"},
	}
	cases = append(cases, case6)

	case7 := testCase{
		"Slices",
		[]Profile {
			{34, "Juelich"},
			{35, "Wuerselen"},
		},
		[]string{"Juelich", "Wuerselen"},
	}
	cases = append(cases, case7)

//	case8 := testCase{
//		"Maps",
//		map[string]string{
//			"Foo": "Bar",
//			"Baz": "Boz",
//		},
//		[]string{"Bar", "Boz"},
//	}
//	cases = append(cases, case8)

	for _, test := range cases{
		t.Run(test.Name, func(t *testing.T) {
			var got []string
			walk(test.Input, func(input string) {
				got = append(got, input)
			})

			if !reflect.DeepEqual(got, test.ExpectedCalls) {
				t.Errorf("got %v, want %v", got, test.ExpectedCalls)
			}
		})
	}

	t.Run("with maps", func(t *testing.T) {
		testMap := map[string]string{
			"Foo": "Bar",
			"Baz": "Boz",
		}

		var got []string
		walk(testMap, func(input string) {
			got = append(got, input)
		})

		assertContains(t, got, "Bar")
		assertContains(t, got, "Boz")
	})

	t.Run("with channels", func(t *testing.T) {
		testChannel := make(chan Profile)

		go func() {
			testChannel <- Profile{34, "Juelich"}
			testChannel <- Profile{30, "Essen"}
			close(testChannel)
		}()

		var got []string
		want := []string{"Juelich", "Essen"}

		walk(testChannel, func(input string) {
			got = append(got, input)
		})

		if !reflect.DeepEqual(got, want) {
			t.Errorf("got %v, want %v", got, want)
		}
	})

	t.Run("with function", func(t *testing.T) {
		testFunction := func() (Profile, Profile) {
			return Profile{34, "Juelich"}, Profile{30, "Essen"}
		}

		var got []string
		want := []string{"Juelich","Essen"}

		walk(testFunction, func(input string) {
			got = append(got, input)
		})

		if !reflect.DeepEqual(got, want) {
			t.Errorf("got %v, want %v", got, want)
		}
	})
}
