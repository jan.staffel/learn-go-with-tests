package arraysSlices

func SumWithFor(numbersArray [5]int) int {
	sum := 0
	for i := 0; i < 5; i++ {
		sum += numbersArray[i]
	}
	return sum
}

func Sum(numbersArray []int) int {
	sum := 0
	for _, number := range numbersArray {
		sum += number
	}
	return sum
}

func SumAllWithoutAppend(numbers ...[]int) []int {
	paramLength := len(numbers)
	sums := make([]int, paramLength)

	for i, numbersArray := range numbers {
		sums[i] = Sum(numbersArray)
	}

	return sums
}

func SumAll(numbers ...[]int) []int {
	var sums []int
	for _, numbersArray := range numbers {
		sums = append(sums, Sum(numbersArray))
	}

	return sums
}

func SumAllTails(numbers ...[]int) []int {
	var sums []int
	for _, numbersArray := range numbers {
		if len(numbersArray) == 0 {
		sums = append(sums, 0)
		continue
	}
//this also works
//		numbersArray[0] = 0
//but we want to learn slicing
		numbersArray = numbersArray[1:]
//this creates a new slice from the second value to the end
		sums = append(sums, Sum(numbersArray))
	}

	return sums
}
