package iteration

import (
	"testing"
	"fmt"
	"math"
)

func TestRepeat(t *testing.T) {
	repeated := Repeat("a", uint(math.Pow(5,1)))
	expected := "aaaaa"

	if repeated != expected {
	t.Errorf("expected %q but got %q", expected, repeated)
	}
}

func ExampleRepeat() {
	repeated := Repeat("lorem ipsum", 2)
	fmt.Println(repeated)
	//Output: lorem ipsumlorem ipsum
	}

//Benchmark the repeat function. Note: The benchmark takes always about ~1.5 seconds. But the iteration count decreases with the number of repertitions
func BenchmarkRepeat(b *testing.B) {
	for i:= 0; i < b.N; i++ {
		Repeat("abcefghijklmnopqr", 2^63)
	}
}
