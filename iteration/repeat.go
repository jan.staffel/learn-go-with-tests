package iteration

//Repeats the given string 5 times
func Repeat(chars string, n uint) string {
	var repeated string
	for i := uint(0); i < n; i++ {
		repeated += chars
	}
	return repeated
}
