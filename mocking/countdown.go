package main

import (
	"fmt"
	"io"
	"os"
	"time"
)

const finalWord = "Go!"

type Sleeper interface {
	Sleep()
}

/*
type DefaultSleeper struct {
	Calls int
}

/*
func (d *DefaultSleeper) Sleep() {
	time.Sleep(time.Second)
	d.Calls++
}
*/

type ConfigurableSleeper struct {
	duration time.Duration
	sleep func(time.Duration)
}

func (c *ConfigurableSleeper) Sleep() {
	c.sleep(c.duration)
}

func main() {
	sleeper := &ConfigurableSleeper{5 * time.Second, time.Sleep}
	Countdown(os.Stdout, 3, sleeper)
}

func Countdown(out io.Writer, start int, sleeper Sleeper) {
	for i := start ; i > 0; i-- {
		sleeper.Sleep()
		fmt.Fprintln(out, i)
	}
	sleeper.Sleep()
	fmt.Fprint(out, finalWord)
}

