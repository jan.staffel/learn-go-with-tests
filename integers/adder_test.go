package integers

import ("testing"
	"fmt"
)

func TestAdder (t *testing.T) {
	sum := Add(2, 2)
	expected := 4

	if sum != expected {
		t.Errorf("exptected %d got %d", expected, sum)
	}
}

func ExampleAdd () {
	sum := Add(1,1)
	fmt.Println(sum)
	// Output: 2
}


