package main

import "fmt"

const englishPrefix = "Hello, "
const frenchPrefix = "Bonjour, "
const spanishPrefix = "Hola, "

const french = "French"
const spanish = "Spanish"

func Hello(name, language string) string {
	var prefix string
	switch language {
	case french:
		prefix = frenchPrefix
	case spanish:
		prefix = spanishPrefix
	default:
		prefix = englishPrefix
	}

	if name == "" {
		name = "World"
	}
	return prefix + name
}

func main() {
    fmt.Println(Hello("Jan", ""))
}
